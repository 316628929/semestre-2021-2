#import matplotlib.pyplot as plt

def objetivo(x,y):
    return(300*x + 150*y)

restricciones = [[.10,.05,10000,"<="],[.12,.12,16000,"<="],[.07,.10,14000,"<="]]

lineas = []

for i in restricciones:
    a = i[0]
    b = i[1]
    c = i[2]
    if a != 0 and b != 0:
        x = c/a
        y = c/b
        lineas.append([[x,0],[0,y]])
    elif a == 0:
        lineas.append([[c,0],[c,1]])
    elif b == 0:
        lineas.append([[0,c],[1,c]])

def line_intersection(line1, line2):
            xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
            ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

            def det(a, b):
                return a[0] * b[1] - a[1] * b[0]

            div = det(xdiff, ydiff)

            d = (det(*line1), det(*line2))
            x = det(d, xdiff) / div
            y = det(d, ydiff) / div
            return([x, y])

puntos = [[0,0]]

for i in lineas:
    for j in lineas:
        if i != j:
            li = line_intersection(i,j)
            if li[0] >= 0 and li[1] >=0:
                if li not in puntos:
                    puntos.append(li)

ejex = []
ejey = []
for i in lineas:
    for j in i:
        if j[0] == 0:
            ejex.append(j[1])
        if j[1] == 0:
            ejey.append(j[0])
for i in ejex:
    puntos.append([0,i])
for i in ejey:
    puntos.append([i,0])

soluciones = []

for i in puntos:
    soluciones.append(objetivo(i[0],i[1]))

sol_pun = []
for i in range(0,len(soluciones)):
    sol_pun.append([soluciones[i],puntos[i]])

def maximizar():
    sp = sorted(sol_pun, reverse=True)
    w = 0
    v = 0
    i = 0
    while w == 0:
        ec = sp[i][1]
        v = 0
        for j in restricciones:
            if j[3] == "=":
                if j[0]*ec[0] + j[1]*ec[1] != j[2]:
                    v = 1
            elif j[3] == "<=":
                if j[0]*ec[0] + j[1]*ec[1] > j[2]:
                    v = 1
            elif j[3] == ">=":
                if j[0]*ec[0] + j[1]*ec[1] < j[2]:
                    v = 1
            elif j[3] == "<":
                if j[0]*ec[0] + j[1]*ec[1] >= j[2]:
                    v = 1
            elif j[3] == ">":
                if j[0]*ec[0] + j[1]*ec[1] <= j[2]:
                    v = 1
        if v == 1:
            i += 1
        elif v == 0:
            w = 1
    return(sp[i])

def minimizar():
    sp = sorted(sol_pun)
    w = 0
    v = 0
    i = 0
    while w == 0:
        ec = sp[i][1]
        v = 0
        for j in restricciones:
            if j[3] == "=":
                if j[0]*ec[0] + j[1]*ec[1] != j[2]:
                    v = 1
            elif j[3] == "<=":
                if j[0]*ec[0] + j[1]*ec[1] > j[2]:
                    v = 1
            elif j[3] == ">=":
                if j[0]*ec[0] + j[1]*ec[1] < j[2]:
                    v = 1
            elif j[3] == "<":
                if j[0]*ec[0] + j[1]*ec[1] >= j[2]:
                    v = 1
            elif j[3] == ">":
                if j[0]*ec[0] + j[1]*ec[1] <= j[2]:
                    v = 1
        if v == 1:
            i += 1
        elif v == 0:
            w = 1
    return(sp[i])

#for j in lineas:
#    point1 = j[0]
#    point2 = j[1]
#    x_values = [point1[0], point2[0]]
#    y_values = [point1[1], point2[1]]
#    plt.plot(x_values, y_values)

#plt.plot(maximizar()[1][0], maximizar()[1][1], marker='o', markersize=3, color="red")
#plt.plot(minimizar()[1][0], minimizar()[1][1], marker='o', markersize=3, color="yellow")
#plt.show()
